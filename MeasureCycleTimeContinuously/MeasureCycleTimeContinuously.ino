#include <Wire.h>
#include <LiquidCrystal_I2C.h>

int sensorPin = A0; // select the input pin for LDR
int sensorValue = 0; // variable to store the value coming from the sensor
const int buttonPin = 2;
const int ldrPin = 8;
const int  d7 = 2;
int buttonState = 0;
int progState = 0;
int measuringState = 0;
int nrOfReadings = 0;
unsigned long startTime = 0;
unsigned long totalTime = 0;
unsigned long elapsedTime = 0;
unsigned long lastVal = 0;
LiquidCrystal_I2C lcd(0x27, 20, 4 ); // Set the LCD address to 0x27 for a 16 chars and 2 line display

void setup() {
  // put your setup code here, to run once:
  pinMode(ldrPin, INPUT);
  pinMode(buttonPin, INPUT);
  Serial.begin(9600); //initialise serial monitor
  // set up the LCD's number of columns and rows:
  lcd.init();                      // initialize the lcd
  lcd.backlight();
  lcd.setCursor(5, 0);
  lcd.print("Ready!");
}

void loop() {
  // put your main code here, to run repeatedly:
  buttonState = digitalRead(buttonPin);
  lcd.setCursor(0, 1);
  lcd.print("Press start btn!");
  while (buttonState == HIGH && progState == 0) { //as long as the button is pressed and program state is 0
    buttonState = digitalRead(buttonPin);
    if (buttonState == HIGH) {
      delay(60); //check debouncing
      buttonState = digitalRead(buttonPin);
      if (buttonState == HIGH) { //if it is still pressed then it is
        Serial.println("Start button is pressed!");
        lcd.clear();
        lcd.setCursor(1, 1);
        lcd.print("Start pressed!");
        progState = 1;
        //int bright = digitalRead(8); //start reading the brightness
        while (progState == 1) { //
          int bright = digitalRead(8);
          lcd.setCursor(0, 0);
          lcd.print("Waiting for next!");
          Serial.println("Waiting!");

          buttonState = digitalRead(buttonPin);
          if (buttonState == LOW && progState == 1) { //check if the button is released
            delay(60);
            buttonState = digitalRead(buttonPin);
            if (buttonState == LOW) {
              Serial.println("Stopped!");
              totalTime = 0;
              nrOfReadings = 0;
              lcd.clear();
              lcd.setCursor(0, 0);
              lcd.print("Last Val:");
              lcd.setCursor(10, 0);
              lcd.print((lastVal) / 1000.0, 2);
              lastVal = 0;
              progState = 0;
            }
          }
          if (bright == LOW &&  measuringState == 0) {
            startTime = millis();
            nrOfReadings++;
            lcd.clear();
            lcd.print("Bright:");
            lcd.setCursor(9, 0);
            lcd.print(nrOfReadings);
            lcd.setCursor(0, 1);
            lcd.print("Timer started!");
            Serial.println("Bright! Timer started!");
            measuringState = 1;

            Serial.println(nrOfReadings);
            delay(100); //delay to avoid detecting one light twice
            
            while (measuringState == 1) {
              bright = digitalRead(8); //measure the second light
              Serial.println("Not bright!");
              if (bright == LOW) {
                lcd.clear();
                Serial.println("Bright again!");
                lcd.print("Bright again!");
                unsigned long currentTime = millis();
                elapsedTime = currentTime - startTime;
                totalTime += elapsedTime;
                unsigned long cycleTime = totalTime / nrOfReadings;
                lastVal = cycleTime;
                Serial.println((cycleTime) / 1000.0, 2);
                lcd.setCursor(0, 1);
                lcd.print((cycleTime) / 1000.0, 2);
                measuringState = 0;
                delay(900); //delay half a sec
              }
            }
          }
        }
      }
    }
  }
}
